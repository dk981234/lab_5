#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
sem_t semphs [3];
pthread_t pthread_1;
pthread_t pthread_2;
char alphabet[26] = "abcdefghijklmnopqrstuvwxyz";
void *start_routine_1()
{
    while(sem_wait(&semphs[0]) == 0)
    {  
        int(*func)(int);
        func = (islower(alphabet[0])) ? &toupper : &tolower;
        int i = 0;
        while(alphabet[i])
        {
	      alphabet[i]=(char)func(alphabet[i]);
            i++;
        }
        sem_post(&semphs[2]);
    }
    return (void *)NULL;
}
void *start_routine_2()
{
    while(sem_wait(&semphs[1]) == 0)
    {
        for(int i = 0; i < (int)sizeof(alphabet) / 2; i++)
        {
            char tmp = alphabet[i];
            alphabet[i] = alphabet[sizeof(alphabet) - 1 - i];
            alphabet[sizeof(alphabet) - 1 - i] = tmp;
        }
        sem_post(&semphs[2]);
    }
    return (void *)NULL;
}
int main ()
{
    for(int i = 0; i < 3; i++)
    {
        sem_init(&semphs[i],0,0);
    }
    pthread_create(&pthread_1, NULL, &start_routine_1, NULL);
    pthread_create(&pthread_1, NULL, &start_routine_2, NULL);
    int i = 0;
    while(1)
    {
        sem_post(&semphs[i % 2]);
        sem_wait(&semphs[2]);
        printf("%s\n", alphabet);
        i++;
        sleep(1);
    }


}