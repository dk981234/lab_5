#pragma once

#include <unistd.h>
#include <time.h>
char *file ="shared";
struct data{
	pid_t pid;
	uid_t uid;
	gid_t gid;
	double time;
	double loadavg[3];
};