#include <unistd.h>
#include <sys/mman.h>
#include <time.h>
#include "data.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
struct data *dt;
void set_init_data()
{
    dt->pid = getpid();
    dt->uid = getuid();
    dt->gid = getgid();
}
void signal_handler(int sig)
{
    switch(sig)
    {
    case SIGHUP:
        printf("Pid:%u\n", (unsigned int)dt->pid);
        break;
    case SIGINT:
        printf("Uid:%u\n", (unsigned int)dt->uid);
        break;
    case  SIGTERM:
        printf("Gid:%u\n", (unsigned int)dt->gid);
        break;
    case  SIGUSR1:
        printf("Time:%g\n", dt->time);
        break;
    case  SIGUSR2:
        printf("Average load(1):%g\n", dt->loadavg[0]);
        printf("Average load(5):%g\n", dt->loadavg[1]);
        printf("Average load(15):%g\n", dt->loadavg[2]);
        break;
    }
}
int main()
{
    time_t st_tm, tm;
    unlink(file);
    time(&st_tm);
    dt = malloc(sizeof(struct data));
    if (dt == NULL)
    {
        perror("malloc function error");
        return 1;
    }
    signal(SIGHUP,&signal_handler);
    signal(SIGINT,&signal_handler);
    signal(SIGTERM,&signal_handler);
    signal(SIGUSR1,&signal_handler);
    signal(SIGUSR2,&signal_handler);
    set_init_data();
    while(1)
    {
        time(&tm);
        dt->time = difftime(tm, st_tm);
        if(getloadavg(dt->loadavg, 3) < 0)
        {
            perror("getloadavg function error");
            return 1;
        }
        sleep(1);
    }

    return 0;
}
