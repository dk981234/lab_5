#!/bin/perl -T
use strict;
$ENV{"PATH"} = "/usr/bin";
my $load_string;
my @loads;
my $pid   = $$;
my $uid   = $<;
my $gid   = $(;
my $st_tm = time();

sub print_avg_loads {
    $load_string = `uptime`;
    $load_string =~ s/.*load average: //g;
    $load_string =~ s/,\s/ /g;
    $load_string =~ s/,/\./g;
    @loads=split( ' ', $load_string);
   	printf( "Average load(1):%g\n", $loads[0] );
   	printf( "Average load(5):%g\n", $loads[1] );
   	printf( "Average load(15):%g\n", $loads[2] );
}
$SIG{HUP}  = sub { printf( "Pid:%u\n", $pid ); };
$SIG{INT}  = sub { printf( "Uid:%u\n", $gid ); };
$SIG{TERM} = sub { printf( "Gid%u\n", $uid ); };
$SIG{USR1} = sub { printf( "Time:%g\n", time() - $st_tm ); };
$SIG{USR2} = \&print_avg_loads;
while (1) {
}
