#include <unistd.h>
#include <sys/mman.h>
#include <time.h>
#include "data.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
void set_init_data(struct data *dt)
{
    dt->pid = getpid();
    dt->uid = getuid();
    dt->gid = getgid();
}
int main()
{
    time_t st_tm, tm;
    unlink(file);
    time(&st_tm);
    int fd = open(file, O_RDWR | O_CREAT, 0644);
    if(fd < 0)
    {
        perror("open function error");
        return 1;
    }
    if(lseek(fd, sizeof(struct data) - 1, SEEK_SET) < 0)
    {
        perror("lseek function error");
        return 1;
    }
    if(write(fd, "", 1) != 1)
    {
         perror("write function error");
         return 1;
    }
    struct data  *dt = (struct data *)mmap(NULL, sizeof(struct data), PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);
    if (dt == MAP_FAILED)
    {
        perror("mmap function error");
        return 1;
    }
    set_init_data(dt);
    while(1)
    {
        time(&tm);
        dt->time = difftime(tm, st_tm);
        if(getloadavg(dt->loadavg, 3) < 0)
        {
            perror("getloadavg function error");
            return 1;
        }
        msync(dt,sizeof(struct data),MS_ASYNC|MS_INVALIDATE);
        sleep(1);
    }
    
    return 0;
}