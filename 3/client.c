#include <unistd.h>
#include <sys/mman.h>
#include "data.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include "data.h"
int main()
{
    int fd = open(file, O_RDONLY);
    if(fd < 0)
    {
        perror("open function error");
        return 1;
    }
    struct data  *dt = (struct data *)mmap(NULL, sizeof(struct data),PROT_READ, MAP_SHARED, fd, 0);
    if (dt == MAP_FAILED)
    {
        perror("mmap function error");
        return 1;
    }
    printf("Pid:%i\nUid:%i\nGid:%i\nTime:%g\nAverage load(1):%g\nAverage load(5):%g\nAverage load(15):%g\n",
    	dt->pid,
    	dt->uid,
    	dt->gid,
    	dt->time,
    	dt->loadavg[0],
    	dt->loadavg[1],
    	dt->loadavg[2]);
}
